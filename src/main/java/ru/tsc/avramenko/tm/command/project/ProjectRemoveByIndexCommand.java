package ru.tsc.avramenko.tm.command.project;

import ru.tsc.avramenko.tm.command.AbstractProjectCommand;
import ru.tsc.avramenko.tm.enumerated.Role;
import ru.tsc.avramenko.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.avramenko.tm.exception.system.ProcessException;
import ru.tsc.avramenko.tm.model.Project;
import ru.tsc.avramenko.tm.util.TerminalUtil;

public class ProjectRemoveByIndexCommand extends AbstractProjectCommand {

    @Override
    public String name() {
        return "project-remove-by-index";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Removing project by index.";
    }

    @Override
    public void execute() {
        System.out.println("ENTER INDEX:");
        final int index = TerminalUtil.nextNumber() - 1;
        final String userId = serviceLocator.getAuthService().getCurrentUserId();
        final Project project = serviceLocator.getProjectService().findByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        final Project removedProject = serviceLocator.getProjectTaskService().removeProjectByIndex(userId, index);
        if (removedProject == null) throw new ProcessException();
        else System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return Role.values();
    }

}