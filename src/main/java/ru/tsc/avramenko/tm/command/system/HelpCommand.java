package ru.tsc.avramenko.tm.command.system;

import ru.tsc.avramenko.tm.command.AbstractCommand;

public class HelpCommand extends AbstractCommand {

    @Override
    public String name() {
        return "help";
    }

    @Override
    public String arg() {
        return "-h";
    }

    @Override
    public String description() {
        return "Display list of commands.";
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        int index = 0;
        for (final AbstractCommand command : serviceLocator.getCommandService().getCommands()) {
            index++;
            System.out.println(index + ". " + command.toString());
        }
    }

}