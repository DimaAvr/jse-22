package ru.tsc.avramenko.tm.command.task;

import ru.tsc.avramenko.tm.command.AbstractTaskCommand;
import ru.tsc.avramenko.tm.enumerated.Role;
import ru.tsc.avramenko.tm.exception.entity.TaskNotFoundException;
import ru.tsc.avramenko.tm.model.Task;
import ru.tsc.avramenko.tm.util.TerminalUtil;

public class TaskFinishByIdCommand extends AbstractTaskCommand {

    @Override
    public String name() {
        return "task-finish-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Finish task by id.";
    }

    @Override
    public void execute() {
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final String userId = serviceLocator.getAuthService().getCurrentUserId();
        final Task task = serviceLocator.getTaskService().finishById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        serviceLocator.getTaskService().finishById(userId, id);
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return Role.values();
    }

}