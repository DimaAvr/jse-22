package ru.tsc.avramenko.tm.command.project;

import ru.tsc.avramenko.tm.command.AbstractProjectCommand;
import ru.tsc.avramenko.tm.enumerated.Role;

public class ProjectClearCommand extends AbstractProjectCommand {

    @Override
    public String name() {
        return "project-clear";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Removing all projects.";
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR PROJECT]");
        final String userId = serviceLocator.getAuthService().getCurrentUserId();
        serviceLocator.getProjectService().clear(userId);
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return Role.values();
    }

}