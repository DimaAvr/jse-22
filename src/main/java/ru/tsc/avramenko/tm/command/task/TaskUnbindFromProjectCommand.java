package ru.tsc.avramenko.tm.command.task;

import ru.tsc.avramenko.tm.command.AbstractTaskCommand;
import ru.tsc.avramenko.tm.enumerated.Role;
import ru.tsc.avramenko.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.avramenko.tm.exception.entity.TaskNotFoundException;
import ru.tsc.avramenko.tm.exception.system.ProcessException;
import ru.tsc.avramenko.tm.model.Project;
import ru.tsc.avramenko.tm.model.Task;
import ru.tsc.avramenko.tm.util.TerminalUtil;

public class TaskUnbindFromProjectCommand extends AbstractTaskCommand {

    @Override
    public String name() {
        return "task-unbind-from-project";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Unbind task from project.";
    }

    @Override
    public void execute() {
        System.out.println("ENTER PROJECT ID:");
        final String projectId = TerminalUtil.nextLine();
        final String userId = serviceLocator.getAuthService().getCurrentUserId();
        final Project project = serviceLocator.getProjectService().findById(userId, projectId);
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("ENTER TASK ID:");
        final String taskId = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().findById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        Task taskFromProject = serviceLocator.getProjectTaskService().unbindTaskById(userId, projectId, taskId);
        if (taskFromProject == null) throw new ProcessException();
        else System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return Role.values();
    }

}