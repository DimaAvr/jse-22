package ru.tsc.avramenko.tm.command.user;

import ru.tsc.avramenko.tm.command.AbstractUserCommand;
import ru.tsc.avramenko.tm.enumerated.Role;
import ru.tsc.avramenko.tm.exception.system.AccessDeniedException;
import ru.tsc.avramenko.tm.util.TerminalUtil;

public class UserRemoveByLoginCommand extends AbstractUserCommand {

    @Override
    public String name() {
        return "user-remove-by-login";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Remove user by login.";
    }

    @Override
    public void execute() {
        System.out.println("ENTER LOGIN: ");
        final String login = TerminalUtil.nextLine();
        final String id = serviceLocator.getUserService().findByLogin(login).getId();
        final String currentUserId = serviceLocator.getAuthService().getCurrentUserId();
        if (id.equals(currentUserId)) throw new AccessDeniedException();
        serviceLocator.getUserService().removeByLogin(login);
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}