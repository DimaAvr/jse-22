package ru.tsc.avramenko.tm.command;

import ru.tsc.avramenko.tm.model.User;

public abstract class AbstractUserCommand extends AbstractCommand{

    protected void showUser(final User user) {
        System.out.println("Id: " + user.getId());
        System.out.println("Login: " + user.getLogin());
        System.out.println("Name: " + user.getFirstName() + " " + user.getMiddleName() + " " + user.getLastName());
        System.out.println("Email: " + user.getEmail());
        System.out.println("Role: " + user.getRole().getDisplayName());
    }

}