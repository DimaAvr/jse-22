package ru.tsc.avramenko.tm.command.user;

import ru.tsc.avramenko.tm.command.AbstractUserCommand;
import ru.tsc.avramenko.tm.enumerated.Role;
import ru.tsc.avramenko.tm.exception.entity.UserNotFoundException;
import ru.tsc.avramenko.tm.exception.system.AccessDeniedException;
import ru.tsc.avramenko.tm.model.User;
import ru.tsc.avramenko.tm.util.TerminalUtil;

public class UserPasswordChangeCommand extends AbstractUserCommand {

    @Override
    public String name() {
        return "user-change-password";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Change the user's password.";
    }

    @Override
    public void execute() {
        System.out.println("ENTER USER ID: ");
        final String userId = TerminalUtil.nextLine();
        final User user = serviceLocator.getUserService().findById(userId);
        if (user == null) throw new UserNotFoundException();
        final String currentUserId = serviceLocator.getAuthService().getCurrentUserId();
        if (!currentUserId.equals(user.getId())) throw new AccessDeniedException();
        System.out.println("ENTER PASSWORD: ");
        final String password = TerminalUtil.nextLine();
        serviceLocator.getUserService().setPassword(userId, password);
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return Role.values();
    }

}