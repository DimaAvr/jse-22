package ru.tsc.avramenko.tm.api;

import ru.tsc.avramenko.tm.model.AbstractEntity;
import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    void add(final E entity);

    void remove(final E entity);

    List<E> findAll();

    void clear();

    E findById(final String id);

    E removeById(final String id);

}
