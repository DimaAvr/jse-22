package ru.tsc.avramenko.tm.api.service;

import ru.tsc.avramenko.tm.enumerated.Role;

public interface IAuthService {

    String getCurrentUserId();

    void setCurrentUserId(String userId);

    boolean isAuth();

    boolean isAdmin();

    void login(String login, String password);

    void logout();

    void checkRoles(Role... roles);

}
